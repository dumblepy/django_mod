from rest_framework import routers
from .apiviews import SampleUserViewSet, SamplePostViewSet


router = routers.DefaultRouter()
router.register(r'sample_users', SampleUserViewSet)
router.register(r'sample_posts', SamplePostViewSet)
