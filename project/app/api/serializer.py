from rest_framework import serializers
from ..models import SampleUser, SamplePost

class SampleUserSerializer(serializers.ModelSerializer):
    """サンプル用UserモデルをDRFで使うための設です"""
    class Meta:
        model = SampleUser
        fields = ('name', 'address', 'bank', 'card', 'company', 'job')

class SamplePostSerializer(serializers.ModelSerializer):
    """サンプル用PostモデルをDRFで使うための設定です"""
    class Meta:
        model = SamplePost
        fields = ('title', 'body', 'posted_on', 'user')
