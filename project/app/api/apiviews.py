import django_filters
from rest_framework import viewsets, filters

from ..models import SampleUser, SamplePost
from .serializer import SampleUserSerializer, SamplePostSerializer

class SampleUserViewSet(viewsets.ModelViewSet):
    queryset = SampleUser.objects.all()
    serializer_class = SampleUserSerializer


class SamplePostViewSet(viewsets.ModelViewSet):
    queryset = SamplePost.objects.all()
    serializer_class = SamplePostSerializer