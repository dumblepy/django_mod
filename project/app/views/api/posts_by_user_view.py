from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response

from app.orator.SamplePost import SamplePost

class PostsByUser(APIView):
    """post list by specific user
    """

    def get(self, request, user_id=1):
        """post list by specific user

        Args:
            user_id (int): user

        Returns:
            [{
                post.id, post.title, post.body, post.posted_on, post.user_id,
                user.name, user.address, user.bank, user.card, user.company, user.job
            }]: posts
        """
        posts = SamplePost.posts_by_user(user_id).serialize()
        return Response(posts)
