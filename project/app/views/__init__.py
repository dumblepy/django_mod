"""ビュー。コントローラーにあたる。
"""
from . import toppage_view
from .api.posts_by_user_view import PostsByUser
