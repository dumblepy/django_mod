from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response

class TopppageView(APIView):
    """Toppage
    """

    def get(self, request):
        """get /
        """
        return render(request, 'app/root.html')
