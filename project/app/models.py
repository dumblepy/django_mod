from django.db import models

# Create your models here.

# Django: モデルフィールドリファレンスの一覧
# https://qiita.com/nachashin/items/f768f0d437e0042dd4b3

class SampleUser(models.Model):
    """サンプル用のUserモデルです
    """

    def __str__(self):
        return str(self.name)
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    bank = models.CharField(max_length=255)
    card = models.CharField(max_length=255)
    company = models.CharField(max_length=255)
    job = models.CharField(max_length=255)

    class Meta:
        db_table = 'sample_users'
        verbose_name_plural = 'sample_users'

class SamplePost(models.Model):
    """サンプル用のPostモデルです
    """

    def __str__(self):
        return str(self.title)
    title = models.CharField(max_length=255)
    body = models.TextField()
    posted_on = models.DateField()
    user = models.ForeignKey(SampleUser, on_delete='PROTECT')

    class Meta:
        db_table = 'sample_posts'
        verbose_name_plural = 'sample_post'
