from django.contrib import admin
from .models import SampleUser, SamplePost

# Register your models here.
admin.site.register(SampleUser)
admin.site.register(SamplePost)

