from orator import DatabaseManager
from orator import Model
from django.conf import settings
import os

BASE_DIR = settings.BASE_DIR
ENGINE = settings.DATABASES['default']['ENGINE'].split('.')[-1]
ENGINE = 'sqlite' if ENGINE == 'sqlite3' else ENGINE
db_file_extension = settings.DATABASES['default']['NAME'].split('.')[-1]
db_file_name = settings.DATABASES['default']['NAME'].split('.')[-2]
NAME = f'{db_file_name}.{db_file_extension}'
USER = settings.DATABASES['default']['USER'] if settings.DATABASES['default']['USER'] else ''
PASSWORD = settings.DATABASES['default']['PASSWORD'] if settings.DATABASES['default']['PASSWORD'] else ''
HOST = settings.DATABASES['default']['HOST'] if settings.DATABASES['default']['HOST'] else ''
PORT = settings.DATABASES['default']['PORT'] if settings.DATABASES['default']['PORT'] else ''
LOG = settings.DEBUG

config = {
    'default': ENGINE,
    'sqlite': {
        'driver': 'sqlite',
        'database': os.path.join(BASE_DIR, NAME),
        'log_queries': LOG,
    },
    'mysql': {
        'driver': 'mysql',
        'host': HOST,
        'database': NAME,
        'port': PORT,
        'user': USER,
        'password': PASSWORD,
        'log_queries': LOG,
    },
    'postgres': {
        'driver': 'postgres',
        'host': HOST,
        'database': NAME,
        'port': PORT,
        'user': USER,
        'password': PASSWORD,
        'log_queries': LOG,
    },
}

db = DatabaseManager(config)
Model.set_connection_resolver(db)
