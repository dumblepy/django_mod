from . import Model

class SampleUser(Model):
    """サンプル用のUserモデルです

    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    bank = models.CharField(max_length=255)
    card = models.CharField(max_length=255)
    company = models.CharField(max_length=255)
    job = models.CharField(max_length=255)
    """
    __table__ = 'sample_users'
