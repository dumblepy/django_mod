from . import Model

class SamplePost(Model):
    """
    Sample posts model

    title = models.CharField(max_length=255)
    body = models.TextField()
    posted_on = models.DateField()
    """
    __table__ = 'sample_posts'

    @staticmethod
    def posts_by_user(user_id):
        """post list by specific user

        Args:
            user_id (int): user

        Returns:
            [{
                post.id, post.title, post.body, post.posted_on, post.user_id,
                user.name, user.address, user.bank, user.card, user.company, user.job
            }]: posts
        """
        return SamplePost \
            .join('sample_users', 'sample_posts.user_id', '=','sample_users.id') \
            .where('sample_users.id', user_id) \
            .get()
