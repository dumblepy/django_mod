from django.urls import path, include

from . import views

api_urls = [
    path('posts_by_user/<int:user_id>', views.PostsByUser.as_view())
]

app_urls = [
    path('api/', include(api_urls)),
    path('', views.toppage_view.TopppageView.as_view())
]
