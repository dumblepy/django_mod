from faker import Faker
import json
import random

# https://faker.readthedocs.io/en/master/locales/ja_JP.html

fake = Faker()

users = []
for i in range(20):
    users += [
        {
            'model': "app.SampleUser",
            'pk': i,
            'fields': {
                'name': fake.name(),
                'address': fake.address(),
                'bank': fake.iban(),
                'card': fake.credit_card_number(card_type=None) + '.' + \
                    fake.credit_card_expire(start="now", end="+10y", date_format="%m/%y") + '.' + \
                    fake.credit_card_full(card_type=None),
                'company': fake.company(),
                'job': fake.job()
            }
        }
    ]

posts = []
for i in range(100):
    num = random.randint(0, 19)
    posts += [
        {
            'model': "app.SamplePost",
            'pk': i,
            'fields': {
                'title': users[num]['fields']['name'] + '---' +fake.sentences(nb=1, ext_word_list=None)[0],
                'body': fake.text(max_nb_chars=200, ext_word_list=None),
                'posted_on': fake.past_date(start_date="-10y", tzinfo=None).strftime('%Y-%m-%d'),
                'user_id': num
            }
        }
    ]

# ファイル作成
with open('project/app/fixture/sample_users.json', 'w', encoding='utf-8') as f:
    json.dump(users, f, ensure_ascii=False, indent=4)

with open('project/app/fixture/sample_posts.json', 'w', encoding='utf-8') as f:
    json.dump(posts, f, ensure_ascii=False, indent=4)
