import sys, os, re

def model():
    class_name = arg2
    try:
        table_name = sys.argv[3]
    except:
        p = """
  [MissingArguments]
  Not enough arguments

  model <ClassName> <table_name>

"""
        print(p)
        sys.exit(0)

    p = f"""from . import Model

class {class_name}(Model):
    __table__ = '{table_name}'

    @staticmethod
    def sample():
        return {class_name}.limit(10).get()
"""
    roadie_path = os.path.dirname(os.path.abspath(__file__))
    model_path = roadie_path + '/app/orator/' + class_name + '.py'

    if not os.path.exists(model_path):
        with open(model_path, 'w') as f:
            f.write(p)
    else:
        print(f'file {class_name}.html is already exists')

#===========================================================

def view():
    # 【技術】pythonでのキャメルケースとスネークケースの変換について
    # http://hatakazu.hatenablog.com/entry/2013/02/16/135911
    # Pythonで大文字・小文字を操作する文字列メソッド一覧
    # https://note.nkmk.me/python-capitalize-lower-upper-title/
    file_name = arg2.split('/')[-1] # test/aaa/test_aaa_bbb -> test_aaa_bbb
    file_name = file_name.capitalize() # test_aaa_bbb -> Test_aaa_bbb
    file_name = re.sub("_(.)",lambda x:x.group(1).upper(),file_name) # Test_aaa_bbb -> TestAaaBbb

    p = f"""from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response

class {file_name}View(APIView):
    def get(self, request):
        pass

    def post(self, request):
        pass

    def patch(self, request):
        pass

    def delete(self, request):
        pass
"""

    file_name = arg2
    roadie_path = os.path.dirname(os.path.abspath(__file__))
    view_path = roadie_path + '/app/views/' + file_name + '_view.py'

    if not os.path.exists(view_path):
        try:
            with open(view_path, 'w') as f:
                f.write(p)
        except:
            file_arr = file_name.split('/')
            dirs = file_arr[0:-1]
            make_dirs = '/'.join(dirs) # 配列を結合して文字列に
            os.makedirs(roadie_path + '/app/views/' + make_dirs, exist_ok=True)
            with open(view_path, 'w') as f:
                f.write(p)
    else:
        print(f'file {file_name}_view.py is already exists')

#===========================================================

def template():
    file_name = arg2
    roadie_path = os.path.dirname(os.path.abspath(__file__))
    template_path = roadie_path + '/app/templates/app/' + file_name + '.html'

    if not os.path.exists(template_path):
        try:
            with open(template_path, 'w') as f:
                f.write('')
        except:
            file_arr = file_name.split('/')
            dirs = file_arr[0:-1]
            make_dirs = '/'.join(dirs) # 配列を結合して文字列に
            os.makedirs(roadie_path + '/app/templates/app/' + make_dirs, exist_ok=True)
            with open(template_path, 'w') as f:
                f.write('')
    else:
        print(f'file {file_name}_view.py is already exists')

#===========================================================

if __name__ == "__main__":

    try:
        arg1 = sys.argv[1]
        arg2 = sys.argv[2]
    except:
        p = """
  [MissingArguments]
  Not enough arguments

  model     <ClassName> <table_name>
  view      <view_name>
  template  <template_name>

  Available subcommands:
    model       Creates a Orator model.
    view        Creates a Django Rest Framework view.
    template    Creates a Jinja2 template.
        """
        print(p)
        sys.exit(0)

    #===========================================================

    if arg2 == '-h' or arg2 == '--help' or arg2 is None:
        if arg1 == 'model':
            p = """
Options:
    -h, --help  Display this help message

model <ClassName> <table_name>

ex: python roadie.py model User user
    => project/app/orator/User.py will be created.
            """
            print(p)
            sys.exit(0)
        elif arg1 == 'view':
            p = """
Options:
    -h, --help  Display this help message

view <view_name>

ex: python roadie.py view user_post
    => project/app/views/user_post_view.py will be created.
            """
            print(p)
            sys.exit(0)
        elif arg1 == 'template':
            p = """
Options:
    -h, --help  Display this help message

template <template_name>

ex: python roadie.py template user_post
    => project/app/templates/app/user_post.html will be created.
            """
            print(p)
            sys.exit(0)

    #===========================================================

    if arg1 == 'model':
        model()
    elif arg1 == 'view':
        view()
    elif arg1 == 'template':
        template()

    #===========================================================
